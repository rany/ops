#!/usr/bin/env bash

cd /opt/services/weechat-matrix-rs/ && make install
cd "$HOME"

exit 0
